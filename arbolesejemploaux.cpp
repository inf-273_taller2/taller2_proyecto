# include <bits/stdc++.h>
 
 # define ll long long

 using namespace std;

 const int N = 1e5 + 10;

 ll A[N], tree [4 * N];

 void build (int id , int L, int R) {
 if(L == R) {
 tree [id] = A[L];
 } else {
 int mid = (L + R) / 2;
 build (2 * id , L, mid);
 build (2 * id + 1, mid + 1, R);
 tree [id] = tree [2 * id] + tree [2 * id + 1];
 }
 }
 void update (int id , int L, int R, int pos , ll val ) {
 if(L == R) {
 tree [id] = val ;
 } else {
 int mid = (L + R) / 2;
 if( pos <= mid ) {
 update (2 * id , L, mid , pos , val );
 } else {
 update (2 * id + 1, mid + 1, R, pos , val );
 }
 tree [id] = tree [2 * id] + tree [2 * id + 1];
 }
 }

 ll query (int id , int L, int R, int l, int r) {
 if(L == l and R == r) {
 return tree [id ];
 }
 int mid = (L + R) / 2;
 if(r <= mid ) {
 return query (2 * id , L, mid , l, r);
 }
 if(l > mid ) {
 return query (2 * id + 1, mid + 1, R, l, r);
 }
 return query (2 * id , L, mid , l, mid ) +
 query (2 * id + 1, mid + 1, R, mid + 1, r);
 }

 int main () {
 ios :: sync_with_stdio (0);
 cin.tie (0); cout .tie (0);
 int test ;
 cin >> test ;
 while (test --) {
 int n, q;
 cin >> n >> q; // Cantidad de elementos y queries
 for(int i = 0; i < n; i++) {
 cin >> A[i];
 }
 build (1, 0, n - 1);
 while (q --) {
 string op;
 cin >> op;
 if(op == "A") {
 int pos ; ll val ;
 cin >> pos >> val;
 update (1, 0, n - 1, pos - 1, val );
 } else {
 int l, r;
 cin >> l >> r;
 cout << query (1, 0, n - 1, l - 1, r - 1) << "\n";
 }
 }
 }
 return 0;
 }
